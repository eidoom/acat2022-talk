#!/usr/bin/env bash

file=custom.bib

bibtool -i $file -s -d -@ -o $file
