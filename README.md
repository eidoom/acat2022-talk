# [acat2022-talk](https://gitlab.com/eidoom/acat2022-talk)

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7760420.svg)](https://doi.org/10.5281/zenodo.7760420)

[Live here](https://eidoom.gitlab.io/acat2022-talk/slides.pdf)

[Event](https://indico.cern.ch/event/1106990/) [contribution](https://indico.cern.ch/event/1106990/contributions/4997230/)

[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)
